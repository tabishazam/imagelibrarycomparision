// ImageLib.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include "pch.h"
#include <iostream>
#include <stdio.h>
#include <filesystem>
#include "OIIOWrapper.h"
#include "FIMWrapper.h"
#include "Timer.h"
#include <fstream>
#include "Constants.h"
#include "Accumulator.h"
#include "ImageLib.h"

using namespace std;
namespace fs = std::filesystem;
#define LOGFILENAME "log.txt"

//Caller function to FIM wrapper
double FIMLoadImage(FIMWrapper &FImage,Timer& t, Accumulator& ac, ofstream& logFile, string FilePath, string targetFilePath, bool writeInternal = false)
{
	double total = 0;
	t.reset();
	//File Load
	if (FImage.LoadFileToList(FilePath))
	{
		ac.AddToLoading(t.elapsed());
		total = total + t.elapsed();
		t.reset();
		int xRes;
		int yRes;
		int channels;
		//File Read to vector array
		vector<unsigned char> pixels = FImage.ReadPixels();
		ac.AddToReadingImage(t.elapsed());
		total = total + t.elapsed();
		t.reset();

		//Cout information
		FImage.ShowInfo(xRes, yRes, channels);

		ac.AddToReadingSpec(t.elapsed());
		total = total + t.elapsed();
		t.reset();

		if (writeInternal)
		{//Write without unwrapping the pointer (direct memory access)
			FImage.WriteFileInternal(targetFilePath);
		}
		else
		{//Write read pixels to file 
			FImage.WriteFile(targetFilePath, xRes, yRes, channels, pixels);
		}

		ac.AddToWriting(t.elapsed());
		total = total + t.elapsed();
		t.reset();

	}
	return total;
}



//Function to OIIO Wrapper
double OIIOLoadImage(OIIOWrapper &openImage, Timer& t,Accumulator& ac, ofstream& logFile, string FilePath, string targetFilePath)
{
	double total = 0;
	t.reset();

	//Load file to memory
	if (openImage.LoadFileToList(FilePath))
	{
		
		ac.AddToLoading(t.elapsed());
		total = total + t.elapsed();
		t.reset();

		int xRes;
		int yRes;
		int channels;
		//Read pixels to array
		vector<unsigned char> pixels = openImage.ReadPixels();
		ac.AddToReadingImage(t.elapsed());
		total = total + t.elapsed();
		t.reset();

		//Cout information
		openImage.ShowInfo(xRes, yRes, channels);
		ac.AddToReadingSpec(t.elapsed());
		total = total + t.elapsed();
		t.reset();

		//Write vector array to file
		openImage.WriteFile(targetFilePath, xRes, yRes, channels, pixels);
		
		ac.AddToWriting(t.elapsed());
		total = total + t.elapsed();
		t.reset();
	}
	return total;
}

void CleanUpTargetFolder(string path)
{
	//Function to iterate over source directory, to add 2K image and 4K image path to vector array
	//Structure of "path" should be : path->(2k,4k)->EXR,JPG,PNG,TGA,TIFF (Where "," indicates siblings)
	for (const auto & entry : fs::directory_iterator(path))
	{
		string entryPath = entry.path().string();

		if (entry.is_directory())
		{
			CleanUpTargetFolder(entryPath);
		}
		else
		{
			remove(entryPath.c_str());
		}
	}
}

void DirectoryIterate(string path,vector<vector<string>>& imagePath2K, vector<vector<string>> & imagePath4K )
{
	//Function to iterate over source directory, to add 2K image and 4K image path to vector array
	//Structure of "path" should be : path->(2k,4k)->EXR,JPG,PNG,TGA,TIFF (Where "," indicates siblings)
	for (const auto & entry : fs::directory_iterator(path))
	{
		string entryPath = entry.path().string();
		
		if (entry.is_directory())
		{
			DirectoryIterate(entryPath,imagePath2K,imagePath4K);
		}
		else
		{
			cout << entryPath << std::endl;
			if (entryPath.find("EXR") < entryPath.length())
			{
				if (entryPath.find("2K") < entryPath.length())
				{
					cout << "2K EXR" << endl;
					imagePath2K[0].push_back(entryPath);
				}
				else if(entryPath.find("4K") < entryPath.length())
				{
					cout << "4K EXR" << endl;
					imagePath4K[0].push_back(entryPath);
				}
			}
			else if (entryPath.find("JPG") < entryPath.length())
			{
				if (entryPath.find("2K") < entryPath.length())
				{
					cout << "2K JPG" << endl;
					imagePath2K[1].push_back(entryPath);
				}
				else if (entryPath.find("4K") < entryPath.length())
				{
					cout << "4K JPG" << endl;
					imagePath4K[1].push_back(entryPath);
				}
			}
			else if (entryPath.find("PNG") < entryPath.length())
			{
				if (entryPath.find("2K") < entryPath.length())
				{
					cout << "2K PNG" << endl;
					imagePath2K[2].push_back(entryPath);
				}
				else if (entryPath.find("4K") < entryPath.length())
				{
					cout << "4K PNG" << endl;
					imagePath4K[2].push_back(entryPath);
				}
			}
			else if (entryPath.find("TGA") < entryPath.length())
			{
				if (entryPath.find("2K") < entryPath.length())
				{
					cout << "2K TGA" << endl;
					imagePath2K[3].push_back(entryPath);
				}
				else if (entryPath.find("4K") < entryPath.length())
				{
					cout << "4K TGA" << endl;
					imagePath4K[3].push_back(entryPath);
				}
			}
			else if (entryPath.find("TIFF") < entryPath.length())
			{
				if (entryPath.find("2K") < entryPath.length())
				{
					cout << "2K TGA" << endl;
					imagePath2K[4].push_back(entryPath);
				}
				else if (entryPath.find("4K") < entryPath.length())
				{
					cout << "4K TGA" << endl;
					imagePath4K[4].push_back(entryPath);
				}
			}
		}
	}
}

void IterateOverImagesInVector(std::vector<std::vector<std::string>> &imagePathVector, std::ofstream &logFile, std::vector<std::string> &extensions, OIIOWrapper &openImage, FIMWrapper &fImage, Accumulator &acOIIO, Accumulator &acFIM, Timer &t)
{
	//Iterate over image folders (2k or 4k) and process their corresponding images 
	int indexOfExtension = 0;
	for (vector<string> fileExtension : imagePathVector)
	{
		logFile << extensions[indexOfExtension] << " Image Loading" << endl;

		//Split to alternate -  do half iteration in one go per library
		std::size_t const half_size = fileExtension.size() / 2;
		vector<string> split_lo(fileExtension.begin(), fileExtension.begin() + half_size);
		vector<string> split_hi(fileExtension.begin() + half_size, fileExtension.end());
		vector<vector<string>> mergeSplit;
		mergeSplit.push_back(split_lo);
		mergeSplit.push_back(split_hi);

		for (int i = 0; i < 2; i++)
		{
			cout << "Performing operations using OIIO" << endl;
			for (string filePath : mergeSplit[i])
			{
				string targetString = filePath;
				targetString.replace(filePath.find("source"), 6, "target\\oiio");
				OIIOLoadImage(openImage, t, acOIIO, logFile, filePath, targetString);
			}
			cout << "Performing operations using FIM" << endl;
			for (string filePath : mergeSplit[i])
			{
				string targetString = filePath;
				targetString.replace(filePath.find("source"), 6, "target\\fim");
				FIMLoadImage(fImage, t, acFIM, logFile, filePath, targetString,true);
			}
		}

		logFile << "OIIO Library" << endl;
		logFile << "Loading Image: " << acOIIO.GetLoadingAverage() << " seconds\n";
		logFile << "Reading Image: " << acOIIO.GetReadingImageAverage() << " seconds\n";
		logFile << "Reading Specs: " << acOIIO.GetReadingSpecAverage() << " seconds\n";
		logFile << "Writing Image: " << acOIIO.GetWritingAverage() << " seconds\n";
		acOIIO.Reset();

		logFile << "FIM Library" << endl;
		logFile << "Loading Image: " << acFIM.GetLoadingAverage() << " seconds\n";
		logFile << "Reading Image: " << acFIM.GetReadingImageAverage() << " seconds\n";
		logFile << "Reading Specs: " << acFIM.GetReadingSpecAverage() << " seconds\n";
		logFile << "Writing Image: " << acFIM.GetWritingAverage() << " seconds\n";
		acFIM.Reset();
		logFile << "------------------------" << endl;

		indexOfExtension++;
	}
}

int main()
{
	//Extensions name - used for logging only
	vector<string> extensions;
	extensions.push_back("EXR");
	extensions.push_back("JPG");
	extensions.push_back("PNG");
	extensions.push_back("TGA");
	extensions.push_back("TIFF");

	//vector of vector of path of images with aforementioned extensions
	vector<vector<string>> imagePath2K; 
	imagePath2K.resize(5);
	vector<vector<string>> imagePath4K;
	imagePath4K.resize(5);
	
	string targetPath = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target";
	CleanUpTargetFolder(targetPath);
	string sourcePath = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\source";
	DirectoryIterate(sourcePath, imagePath2K, imagePath4K);


	Timer t;
	OIIOWrapper openImage;
	FIMWrapper fImage;
	ofstream logFile;
	//Accumulator class - used for averaging time elapsed
	Accumulator acOIIO;
	Accumulator acFIM;
	double totalInOiio = 0;
	double totalInFIM = 0;
	logFile.open(LOGFILENAME,ios::out | ios::trunc);

	
	
	logFile << "START OF FILE" << endl;
	//2K Image Loading
	logFile << "------------2K Images------------" << endl;
	IterateOverImagesInVector(imagePath2K, logFile, extensions, openImage, fImage, acOIIO, acFIM, t);
	logFile << "------------4K Images------------" << endl;
	IterateOverImagesInVector(imagePath4K, logFile, extensions, openImage, fImage, acOIIO, acFIM, t);
	

	logFile.close();
}

