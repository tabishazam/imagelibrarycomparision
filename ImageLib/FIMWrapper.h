#pragma once
#include <vector>
#include <iostream>
#include <string>
#include <filesystem>
#include <FreeImage.h>
using namespace std;

class FIMWrapper
{
private:
	vector<unique_ptr<FIBITMAP>> loadedImages;
	vector<string> extensionBox;
	void InitializeDefaultExtensions();
public:
	FIMWrapper();


	bool LoadFileToList(string fileName);
	void DeduceFormatCustom(std::string &filename, FREE_IMAGE_FORMAT &fif, int &flag, int priority);
	void DeduceFormat(std::string &filename, FREE_IMAGE_FORMAT& fif, int &flag );
	vector<unsigned char> ReadPixels(int index = -1, bool showOutput = false);
	void ShowInfo(int& xRes, int& yRes, int& channel, int index = -1);
	void WriteFile(string fileName, int xRes, int yRes, int channel, vector<unsigned char> pixels);
	void WriteFileInternal(string fileName,int index = -1);
	void MovePointerToArray(int index, unique_ptr<FIBITMAP>& in);
	void MoveArrayToPointer(int index, unique_ptr<FIBITMAP>& in);
	~FIMWrapper();


};

