#pragma once

void IterateOverImagesInVector(std::vector<std::vector<std::string>> &imagePath2K, std::ofstream &logFile, std::vector<std::string> &extensions, int &indexOfExtension, double &totalInOiio, OIIOWrapper &openImage, Timer &t, Accumulator &acOIIO, FIMWrapper &fImage, Accumulator &acFIM);
