#pragma once
#include <iostream>
#include <string>

using namespace std;
class Constants
{
public:

	string quixeltifFilePath = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\source\\Quixel_Preview_Orb_Curvature.tif";
	string tifFilePath = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\source\\tiff.tiff";
	string pngFilePath = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\source\\pngimage.png";
	string tgaFilePath = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\source\\targa.tga";
	string jpegFilePath = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\source\\jpeg.jpg";
	string hdrFilePath = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\source\\hdr.hdr";

	string normal2K = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\2K\\ProShape_2K_normal.png";
	string roughness2k = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\2K\\ProShape_2K_roughness.png";
	string displacementEXR2k = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\2K\\ProShape_2K_displacement.exr";

	string normal4K = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\4K\\tcigfcgn_4K_Normal.jpg";
	string albedo4k = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\4K\\tcigfcgn_4K_Albedo.jpg";
	string displacementEXR4k = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\4K\\tcigfcgn_4K_Displacement.exr";

	string targetQuixeltifFilePathOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\oiio\\Quixel_Preview_Orb_Curvature.tif";
	string targetTifFilePathOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\oiio\\tiff.tiff";
	string targetPngFilePathOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\oiio\\pngimage.png";
	string targetTgaFilePathOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\oiio\\targa.tga";
	string targetJpegFilePathOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\oiio\\jpeg.jpg";
	string targetHdrFilePathOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\oiio\\hdr.hdr";

	string targetQuixeltifFilePathFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\fim\\Quixel_Preview_Orb_Curvature.tif";
	string targetTifFilePathFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\fim\\tiff.tiff";
	string targetPngFilePathFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\fim\\pngimage.png";
	string targetTgaFilePathFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\fim\\targa.tga";
	string targetJpegFilePathFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\fim\\jpeg.jpg";
	string targetHdrFilePathFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target\\fim\\hdr.hdr";

	string normal2KtargetOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target2k\\oiio\\ProShape_2K_normal.png";
	string normal2KtargetFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target2k\\fim\\ProShape_2K_normal.png";

	string roughness2KtargetOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target2k\\oiio\\ProShape_2K_roughness.png";
	string roughness2KtargetFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target2k\\fim\\ProShape_2K_roughness.png";

	string displacementEXR2KtargetOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target2k\\oiio\\ProShape_2K_displacement.exr";
	string displacementEXR2KtargetFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target2k\\fim\\ProShape_2K_displacement.exr";

	string normal4KtargetOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target4k\\oiio\\tcigfcgn_4K_Normal.jpg";
	string normal4KtargetFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target4k\\fim\\tcigfcgn_4K_Normal.jpg";

	string albedo4KtargetOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target4k\\oiio\\tcigfcgn_4K_Albedo.jpg";
	string albedo4KtargetFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target4k\\fim\\tcigfcgn_4K_Albedo.jpg";

	string displacementEXR4KtargetOIIO = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target4k\\oiio\\tcigfcgn_4K_Displacement.exr";
	string displacementEXR4KtargetFIM = "D:\\Tabish\\Desktop\\Desktop Archive\\ImageSample\\target4k\\fim\\tcigfcgn_4K_Displacement.exr";
};