#include "pch.h"
#include "OIIOWrapper.h"


OIIOWrapper::OIIOWrapper()
{
}


OIIOWrapper::~OIIOWrapper()
{
}

bool OIIOWrapper::LoadFileToList(string fileName)
{
	
	auto in = ImageInput::open(fileName);
	if (!in)
	{
		cout<<"Error loading file "<<fileName<<" : "<<OIIO::geterror()<<endl;
		return false;
	}
	else
	{
		cout << "File loaded : " << fileName << endl;
		loadedImages.push_back(move(in));
	}
	return true;
}

vector<unsigned char> OIIOWrapper::ReadPixels(int index,bool showOutput)
{
	unique_ptr<ImageInput> in;
	MoveArrayToPointer(index, in);
	vector<unsigned char> pixels;
	if (in)
	{
		const ImageSpec &spec = in->spec();
		int xres = spec.width;
		int yres = spec.height;
		int channels = spec.nchannels;
		pixels.resize(xres*yres*channels);

		in->read_image(TypeDesc::UINT8, &pixels[0]);
		in->close();
		MovePointerToArray(index, in);
	}
	return pixels;
}

void OIIOWrapper::ShowInfo(int& xRes, int& yRes, int& channel, int index)
{
	unique_ptr<ImageInput> in;
	MoveArrayToPointer(index, in);

	if (in)
	{

		const ImageSpec &spec = in->spec();
		xRes = spec.width;
		yRes = spec.height;
		channel = spec.nchannels;
		TypeDesc format = spec.format;

		cout << "-- X Resolution: " << xRes << ", Y Resolution: " << yRes << ", Channels: " << channel << endl;
		MovePointerToArray(index, in);
	}
	else
	{
		cout << "No image loaded " << endl;
	}
}

void OIIOWrapper::WriteFile(string fileName,int xRes,int yRes,int channel, vector<unsigned char> pixels)
{
	if (pixels.size() == 0)
	{
		cout << "Array size is 0" << endl;
		return;
	}
	std::unique_ptr<ImageOutput> out = ImageOutput::create(fileName);
	if (!out)
	{
		return;
	}
	cout << " Writing file :" << fileName << endl;
	ImageSpec spec(xRes, yRes, channel, TypeDesc::UINT8);
	out->open(fileName, spec);
	out->write_image(TypeDesc::UINT8, pixels.data());
	out->close();
}



void OIIOWrapper::MoveArrayToPointer(int index, unique_ptr<ImageInput> &in)
{
	if (index != -1)
	{
		if (index >= 0 && index < loadedImages.size())
			in = move(loadedImages[index]);
	}
	else
	{
		in = move(loadedImages[loadedImages.size() - 1]);
	}
}

void OIIOWrapper::MovePointerToArray(int index, unique_ptr<ImageInput>& in)
{
	if (index != -1)
	{
		loadedImages[index] = move(in);
	}
	else
	{
		loadedImages[loadedImages.size() - 1] = move(in);
	}
}