#pragma once
#include <memory>
#include <vector>
#include <iostream>
#include <string>
#include <OpenImageIO/imageio.h>
using namespace OIIO;
using namespace std;
class OIIOWrapper
{
private:
	vector<unique_ptr<ImageInput>> loadedImages;

public:
	OIIOWrapper();
	bool LoadFileToList(string);
	vector<unsigned char> ReadPixels(int index=-1,bool showOutput = false);
	void ShowInfo(int& xRes,int& yRes,int& channel,int index=-1);
	void WriteFile(string fileName, int xRes, int yRes, int channel, vector<unsigned char> pixels);
	
	void MovePointerToArray(int index, unique_ptr<ImageInput>& in);
	void MoveArrayToPointer(int index, unique_ptr<ImageInput>& in);
	~OIIOWrapper();
};

