#pragma once
#include <iostream>
#include <string>
#include <vector>
using namespace std;
class Accumulator
{
private:
	vector<double> _loadingImage;
	vector<double> _readingImage;
	vector<double> _readingSpec;
	vector<double> _writingImage;
	int elements = 0;
	double Average(vector<double> vec)
	{
		double total = 0;
		
		for (auto& elem : vec)
		{
			total = total + elem;
		}
		return (total / vec.size());
	}

public:
	Accumulator()
	{
		elements = 0;
	}
	void AddToLoading(double value)
	{
		elements++;
		_loadingImage.push_back(value);
	}
	void AddToReadingImage(double value)
	{
		_readingImage.push_back(value);
	}
	void AddToReadingSpec(double value)
	{
		_readingSpec.push_back(value);
	}
	void AddToWriting(double value)
	{
		_writingImage.push_back(value);
	}

	double GetLoadingAverage()
	{
		return Average(_loadingImage);
	}
	double GetReadingImageAverage()
	{
		return Average(_readingImage);
	}
	double GetReadingSpecAverage()
	{
		return Average(_readingSpec);
	}
	double GetWritingAverage()
	{
		return Average(_writingImage);
	}

	void Reset()
	{
		elements = 0;
		 _loadingImage.clear();
		 _readingImage.clear();
		 _readingSpec.clear();
		_writingImage.clear();
	}

	
};