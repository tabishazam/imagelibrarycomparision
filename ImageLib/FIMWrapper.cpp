#include "pch.h"
#include "FIMWrapper.h"


FIMWrapper::FIMWrapper()
{
	InitializeDefaultExtensions();
}

void FIMWrapper::InitializeDefaultExtensions()
{

	extensionBox.push_back(".png");
	extensionBox.push_back(".jpg");
	extensionBox.push_back(".tif");
	extensionBox.push_back(".tiff");
	extensionBox.push_back(".hdr");
	extensionBox.push_back(".tga");
	extensionBox.push_back(".exr");
}

void FIMWrapper::MovePointerToArray(int index, unique_ptr<FIBITMAP>& in)
{
	if (index != -1)
	{
		loadedImages[index] = move(in);
	}
	else
	{
		loadedImages[loadedImages.size() - 1] = move(in);
	}
}

void FIMWrapper::MoveArrayToPointer(int index, unique_ptr<FIBITMAP>& in)
{
	if (index != -1)
	{
		if (index >= 0 && index < loadedImages.size())
			in = move(loadedImages[index]);
	}
	else
	{
		in = move(loadedImages[loadedImages.size() - 1]);
	}
}

FIMWrapper::~FIMWrapper()
{
}

bool FIMWrapper::LoadFileToList(string filename)
{
	FREE_IMAGE_FORMAT fif;
	int flag = 0;
	int priority = 0;
	unique_ptr<FIBITMAP> in;


	DeduceFormat(filename, fif, flag);

	unique_ptr<FIBITMAP> freeImageLoader(FreeImage_Load(fif, filename.c_str(), flag));
	in = move(freeImageLoader);

	if (!in || !FreeImage_FIFSupportsReading(fif))
	{
		cout << "Error: cannot open file: " << filename << endl;
		return false;
	}
	else 
	{
		//Successfully loaded image is saved in vector array
		cout << "File loaded : " << filename << endl;
		loadedImages.push_back(move(in));
	}
	
	return true;
}

//Helper non member funtion to find a value in vector
template < typename T>
std::pair<bool, int > findInVector(const std::vector<T>  & vecOfElements, const T  & element)
{
	std::pair<bool, int > result;

	// Find given element in vector
	auto it = std::find(vecOfElements.begin(), vecOfElements.end(), element);

	if (it != vecOfElements.end())
	{
		result.second = distance(vecOfElements.begin(), it);
		result.first = true;
	}
	else
	{
		result.first = false;
		result.second = -1;
	}

	return result;
}

void FIMWrapper::DeduceFormatCustom(std::string &filename, FREE_IMAGE_FORMAT &fif, int &flag,int priority)
{

	string extension = filesystem::path(filename).extension().string();
	transform(extension.begin(), extension.end(), extension.begin(), ::tolower);

	int index;
	std::pair<bool, int > findResult = findInVector(extensionBox, extension);
	if (findResult.first)
	{
		index = findResult.second;
	}
	int testingIndex = index + priority;
	string correspondingFormat = extensionBox[testingIndex % extensionBox.size()];

	if (correspondingFormat == ".png")
	{
		fif = FIF_PNG;
		flag = PNG_DEFAULT;
	}
	else if (correspondingFormat == ".jpg")
	{
		fif = FIF_JPEG;
		flag = JPEG_DEFAULT;
	}
	else if (correspondingFormat == ".tiff" || extension == ".tif")
	{
		fif = FIF_TIFF;
		flag = TIFF_DEFAULT;
	}
	else if (correspondingFormat == ".hdr")
	{
		fif = FIF_HDR;
		flag = HDR_DEFAULT;
	}
	else if (correspondingFormat == ".tga")
	{
		fif = FIF_TARGA;
		flag = TARGA_DEFAULT;
	}
	else if (correspondingFormat == ".exr")
	{
		//TODO : Fix exr
		fif = FIF_EXR;
		flag = EXR_DEFAULT;
	}
}

void FIMWrapper::DeduceFormat(std::string &filename, FREE_IMAGE_FORMAT &fif, int &flag)
{
	fif = FreeImage_GetFileType(filename.c_str(), 0);
	if (fif == FIF_UNKNOWN)
	{
		cout << "Cannot get filetype from signature, trying extension" << endl;
		// If we can't get the signature, try to guess the file format from the file extension
		fif = FreeImage_GetFIFFromFilename(filename.c_str());
	}
}

std::vector<unsigned char> FIMWrapper::ReadPixels(int index /*= -1*/, bool showOutput /*= false*/)
{
	unique_ptr<FIBITMAP> in;
	MoveArrayToPointer(index, in);
	vector<unsigned char> pixels;
	if (in)
	{

		int xres = FreeImage_GetWidth(in.get());
		int yres = FreeImage_GetHeight(in.get());
		int m_bpp = FreeImage_GetBPP(in.get());
		int channels = m_bpp / 8;
	
		
		pixels.resize(xres*yres*channels);
		memcpy(&pixels[0], in->data, xres*yres*channels);
		
		MovePointerToArray(index, in);
	}
	return pixels;
}

void FIMWrapper::ShowInfo(int& xRes, int& yRes, int& channel, int index /*= -1*/)
{
	unique_ptr<FIBITMAP> in;
	MoveArrayToPointer(index, in);

	if (in)
	{
		xRes = FreeImage_GetWidth(in.get());
		yRes = FreeImage_GetHeight(in.get());
		int m_bpp = FreeImage_GetBPP(in.get());
		channel = m_bpp / 8;

		cout << "--X Resolution: " << xRes << ", Y Resolution: " << yRes << ", Channels: " << channel << endl;
		MovePointerToArray(index, in);
	}
	else
	{
		cout << "No image loaded " << endl;
	}
}

void FIMWrapper::WriteFile(string fileName, int xRes, int yRes, int channel, vector<unsigned char> pixels)
{
	
	bool bCanSave = false;
	int bpp = channel * 8;

	if (pixels.size() == 0)
	{
		cout << "Array size is 0" << endl;
		return;
	}
	
	std::unique_ptr<FIBITMAP> out(FreeImage_Allocate(xRes, yRes, bpp));
	if (!out)
	{
		return;
	}
	FREE_IMAGE_FORMAT fif;
	int flag = 0;
	int priority = 0;

	DeduceFormat(fileName, fif, flag);

	bCanSave = (FreeImage_FIFSupportsWriting(fif) && FreeImage_FIFSupportsExportBPP(fif, bpp));
	if (bCanSave)
	{
		memcpy(out->data, &pixels[0], xRes*yRes*channel);
		FreeImage_Save(fif, out.get(), fileName.c_str(), flag);
	}
	else
	{
		cout << " Error : cannot save file " << fileName << ", format not supported" << endl;
	}
}

void FIMWrapper::WriteFileInternal(string fileName ,int index /*= -1*/)
{
	unique_ptr<FIBITMAP> in;
	MoveArrayToPointer(index, in);

	FREE_IMAGE_FORMAT fif;
	int flag = 0;
	int priority = 0;

	DeduceFormat(fileName, fif, flag);

	BOOL bCanSave;

	FREE_IMAGE_TYPE image_type = FreeImage_GetImageType(in.get());
	if (image_type == FIT_BITMAP) {
		// standard bitmap type
		WORD bpp = FreeImage_GetBPP(in.get());
		bCanSave = (FreeImage_FIFSupportsWriting(fif) &&
			FreeImage_FIFSupportsExportBPP(fif, bpp));
	}
	else {
		// special bitmap type
		bCanSave = FreeImage_FIFSupportsExportType(fif, image_type);
	}

	if (in && bCanSave)
	{
		cout << " Writing file :" << fileName<< endl;
		FreeImage_Save(fif, in.get(), fileName.c_str(), flag);
	}
	else
	{
		cout << " Error : cannot save file " << fileName << ", file not read or format not supported" << endl;
	}
}
